package com.example.acercamientokotlin

fun main(){

    var numero = 2;

    when (numero){
        1 -> println("El numero es 1");
        2-> println("El numero es 2");
        else -> println("El numero es diferente de 1 y 2")

    }
}