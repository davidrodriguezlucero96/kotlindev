package com.example.acercamientokotlin

fun main (){

    var num1 = 1
    var num2 = 20

    for (i in 1..20){
        print(i)
    }

    println(" ------------ ")

    for (i in 1..20 step 2){
        print(i)
    }
    
    println(" ------------ ")

    for (i in num1 until num2 step 3){
        print(i)
    }
}